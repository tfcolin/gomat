package gomat

import "testing"
import "fmt"
import "math"

func TestGesv (t *testing.T) {

	size := 3
	nrhs := 2

	mat := []float64 {1, -1, 2, 2, -2, -1, 3, 0, -3}
	x := []float64{-1, 1, 2, 1, 2, 3}
	b := make ([]float64, size * nrhs)

	for i := 0; i < size; i ++ {
		for j := 0; j < nrhs; j ++ {
			b[j * size + i] = 0
			for k := 0; k < size; k ++ {
				b[j * size + i] += mat[k * size + i] * x[j * size + k]
			}
		}
	}

	fmt.Println ("rhs:", b)

	res := SolveLinearEquations (size, mat, nrhs, b)

	fmt.Println ("res:", b, res)
	fmt.Println ("ori:", x)

	for i := 0; i < nrhs; i ++ {
		for j := 0; j < size; j ++ {
			if math.Abs(x[i * size + j] - b[i * size + j]) > 1e-12 {
				t.Fail()
			}
		}
	}

}

func TestGemv (t *testing.T) {
	mat := []float64 {1, -1, 2, 2, -2, -1, 3, 0, -3}
	x := []float64{-1, 1, 2}
	y1e := make ([]float64, 3)
	y2e := make ([]float64, 3)
	y1 := make ([]float64, 3)
	y2 := make ([]float64, 3)

	for i := 0; i < 3; i ++ {
		y1e[i] = 0
		y2e[i] = 0
		for j := 0; j < 3; j ++ {
			y1e[i] += mat [j * 3 + i] * x[j]
			y2e[i] += mat [i * 3 + j] * x[j]
		}
	}

	MatVec (3, mat, x, y1)
	MatTransVec (3, mat, x, y2)

	fmt.Println ("y1e = ", y1e)
	fmt.Println ("y1 = ", y1)
	fmt.Println ("y2e = ", y2e)
	fmt.Println ("y2 = ", y2)

	for i := 0; i < 3; i ++ {
		if math.Abs(y1e[i] - y1[i]) > 1e-12 {
			t.Fail()
		}
		if math.Abs(y2e[i] - y2[i]) > 1e-12 {
			t.Fail()
		}
	}

}
